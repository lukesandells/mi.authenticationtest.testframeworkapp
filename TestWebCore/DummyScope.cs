﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestWebCore
{
    public class DummyScope : IDisposable
    {
        public string DoSomething()
        {
            return("Not doing anything");
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}

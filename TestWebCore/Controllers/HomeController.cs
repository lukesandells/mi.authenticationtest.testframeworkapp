﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MI.AuthenticationTest.Models;
using MI.Framework.Persistence;
using TestWebCore.Models;

namespace TestWebCore.Controllers
{
    public class HomeController : Controller
    {
        private readonly PersistenceScope _scope;
        private readonly DummyScope _dummy;

        public HomeController(PersistenceScope scope, DummyScope dummy)
        {
            _scope = scope;
            _dummy = dummy;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            var nothing = _dummy.DoSomething();
            var message = _scope.Query<Organisation>().FirstOrDefault(o => o.Name.Equals("NameTest"))?.Name;
            ViewData["Message"] = string.IsNullOrWhiteSpace(message) ? "Not found" : message;

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

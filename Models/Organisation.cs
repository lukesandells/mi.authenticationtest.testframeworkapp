﻿using System;

namespace MI.AuthenticationTest.Models
{
    public class Organisation
    {
        public virtual int OrganisationId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Domain { get; set; }
        public virtual Guid InternalId { get; set; }

        public Organisation()
        {
            
        }

        public Organisation(string name, string domain, Guid internalId)
        {
            Name = name;
            Domain = domain;
            InternalId = internalId;
        }
    }
}
﻿using System;

namespace MI.AuthenticationTest.Models
{
    public abstract class Parameter
    {
        public virtual int ParameterId { get; set; }
        public virtual Guid InternalId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual string Discriminator => this.GetType().ToString();

        public virtual ParameterType Type { get; set; }

        public Parameter()
        {

        }

        public Parameter(string name, string description, Guid internalId, ParameterType type)
        {
            Name = name;
            Description = description;
            InternalId = internalId;
            Type = type;
        }
    }
}

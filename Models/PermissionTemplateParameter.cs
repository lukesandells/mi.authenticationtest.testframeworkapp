﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MI.AuthenticationTest.Models
{
    public class PermissionTemplateParameter : Parameter
    {
        public virtual PermissionTemplate ParentPermissionTemplate { get; set; }
        public virtual string DefaultValue { get; set; }

        public PermissionTemplateParameter()
        {
            DefaultValue = "";
        }

        public PermissionTemplateParameter(string name, string description, Guid internalId, ParameterType type, string defaultValue) : base(name, description, internalId, type)
        {
            DefaultValue = defaultValue;
        }
    }
}

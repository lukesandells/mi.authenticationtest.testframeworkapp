﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MI.AuthenticationTest.Models
{
    public class RoleAssignmentParameter : Parameter
    {
        public virtual RoleAssignment ParentRoleAssignment { get; set; }
        public virtual string Value { get; set; }

        public RoleAssignmentParameter()
        {
            Value = "";
        }

        public RoleAssignmentParameter(string name, string description, Guid internalId, ParameterType type, string value) : base(name, description, internalId, type)
        {
            Value = value;
        }
    }
}

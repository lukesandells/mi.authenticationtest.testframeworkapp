﻿using System;
using System.Collections.Generic;

namespace MI.AuthenticationTest.Models
{
    public class PermissionAssignment
    {
        public virtual int PermissionAssignmentId { get; set; }
        public virtual string Name { get; set; }
        public virtual string ClaimType { get; set; }
        public virtual string Description { get; set; }
        public virtual Guid InternalId { get; set; }
        //this is provided for reporting rather than operational use
        public virtual PermissionTemplate PermissionTemplate { get; set; }

        public virtual RoleAssignment RoleAssignment { get; set; }  //nullable
        public virtual ISet<PermissionAssignmentParameter> PermissionParameters { get; set; } = new HashSet<PermissionAssignmentParameter>();

        public PermissionAssignment()
        {

        }

        public PermissionAssignment(string name, string claimType, string description, Guid internalId, PermissionTemplate permissionTemplate)
        {
            Name = name;
            ClaimType = claimType;
            Description = description;
            InternalId = internalId;
            PermissionTemplate = permissionTemplate;
        }

        public PermissionAssignment(string name, string claimType, string description, Guid internalId, PermissionTemplate permissionTemplate, ISet<PermissionAssignmentParameter> permissionParameters, RoleAssignment roleAssignment = null) 
            : this(name, claimType, description, internalId, permissionTemplate)
        {
            //foreach(var pap in permissionParameters)
            //{
            //    pap.ParentPermissionAssignment = this;
            //}

            PermissionParameters.UnionWith(permissionParameters);

            RoleAssignment = roleAssignment;
        }
    }
}

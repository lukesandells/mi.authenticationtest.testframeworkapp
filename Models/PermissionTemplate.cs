﻿using System;
using System.Collections.Generic;

namespace MI.AuthenticationTest.Models
{
    public class PermissionTemplate
    {
        public virtual int PermissionTemplateId { get; set; }
        public virtual Guid InternalId { get; set; }
        public virtual string Name { get; set; }
        public virtual string ClaimType { get; set; }
        public virtual string Description { get; set; }

        //public virtual RoleTemplate RoleTemplate { get; set; }  //in the db this will be nullable
        public virtual ISet<PermissionTemplateParameter> TemplateParameters { get; set; } = new HashSet<PermissionTemplateParameter>();
        
        public PermissionTemplate()
        {

        }

        public PermissionTemplate(string name, string claimType, string description, Guid internalId)
        {
            Name = name;
            ClaimType = claimType;
            Description = description;
            InternalId = internalId;
        }

        public PermissionTemplate(string name, string claimType, string description, Guid internalId, ISet<PermissionTemplateParameter> templateParameters/*, RoleTemplate roleTemplate = null*/) 
            : this(name, claimType, description, internalId)
        {
            //foreach(PermissionTemplateParameter ptp in templateParameters)
            //{
            //    ptp.ParentPermissionTemplate = this;
            //}

            TemplateParameters.UnionWith(templateParameters);

            //RoleTemplate = roleTemplate;
        }
    }
}

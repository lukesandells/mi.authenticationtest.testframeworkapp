﻿using System;
using System.Collections.Generic;

//using System.ComponentModel.DataAnnotations.Schema;

namespace MI.AuthenticationTest.Models
{
    public class RoleTemplate
    {
        public virtual int RoleTemplateId { get; set; }
        public virtual Guid InternalId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }

        //either a list here, or a single object in the PermissionTemplate
        public virtual ISet<PermissionTemplate> PermissionTemplates { get; set; } = new HashSet<PermissionTemplate>();
        public virtual ISet<RoleTemplateParameter> ParameterTemplates { get; set; } = new HashSet<RoleTemplateParameter>();

        public RoleTemplate()
        {

        }

        public RoleTemplate(string roleTemplateName, string description, Guid internalId)
        {
            Name = roleTemplateName;
            Description = description;
            InternalId = internalId;
        }

        public RoleTemplate(string roleTemplateName, string description, Guid internalId, ISet<PermissionTemplate> permissionTemplates, ISet<RoleTemplateParameter> parameterTemplates)
            : this(roleTemplateName, description, internalId)
        {
            //foreach (RoleTemplateParameter rtp in parameterTemplates)
            //{
            //    rtp.ParentRoleTemplate = this;
            //}
            
            PermissionTemplates.UnionWith(permissionTemplates);
            ParameterTemplates.UnionWith(parameterTemplates);
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace MI.AuthenticationTest.Models
{
    public abstract class ParameterType
    {
        public virtual int ParameterTypeId { get; set; }
        public virtual Guid InternalId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual string TypeDiscriminator => this.GetType().ToString();
        //public abstract string GetParameterStringValue(Parameter parameter);

        public ParameterType()
        {

        }

        public ParameterType(string name, string description, Guid internalId)
        {
            Name = name;
            Description = description;
            InternalId = internalId;
        }
    }

    public abstract class ParameterType<T> : ParameterType
    {
        //public virtual int ParameterTypeId { get; set; }
        //public virtual Guid InternalId { get; set; }
        //public virtual string Name { get; set; }
        //public virtual string Description { get; set; }
        //public virtual string TypeDiscriminator => this.GetType().ToString();
        //public virtual T Value { get; set; }

        
        public ParameterType()
        {

        }

        public ParameterType(string name, string description, Guid internalId)
            : base(name, description, internalId)
        {
            //Value = value;
        }
    }
}
﻿using System;

namespace MI.AuthenticationTest.Models
{
    public class Site
    {
        public virtual int SiteId { get; set; }
        public virtual Guid InternalId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }

        public virtual Organisation Organisation { get; set; }

        public Site()
        {

        }

        public Site(string name, string description, Guid internalId, Organisation organisation)
        {
            Name = name;
            Description = description;
            InternalId = internalId;
            Organisation = organisation;
        }
    }
}
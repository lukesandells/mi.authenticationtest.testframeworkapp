﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MI.AuthenticationTest.Models
{
    public class RoleTemplateParameter : Parameter
    {
        public virtual RoleTemplate ParentRoleTemplate { get; set; }
        public virtual string DefaultValue { get; set; }

        public RoleTemplateParameter()
        {
            DefaultValue = "";
        }

        public RoleTemplateParameter(string name, string description, Guid internalId, ParameterType type, string defaultValue) : base(name, description, internalId, type)
        {
            DefaultValue = defaultValue;
        }
    }
}

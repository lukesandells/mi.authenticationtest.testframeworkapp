﻿using System;
using System.Collections.Generic;

namespace MI.AuthenticationTest.Models
{
    public class ApplicationUser
    {
        public virtual int UserId { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual bool IsEnabled { get; set; }
        public virtual string Email { get; set; }
        public virtual Guid InternalId { get; set; }
        public virtual string PasswordHash { get; set; }
        public virtual string PasswordSalt { get; set; }

        public virtual Organisation Organisation { get; set; }

        public virtual ISet<PermissionAssignment> Permissions { get; set; } = new HashSet<PermissionAssignment>();
        public virtual ISet<RoleAssignment> Roles { get; set; } = new HashSet<RoleAssignment>();

        public ApplicationUser()
        { }

        public ApplicationUser(string firstName, string lastName, bool isEnabled, string email, Guid internalId, Organisation organisation, string passwordHash, string passwordSalt)
        {
            FirstName = firstName;
            LastName = lastName;
            IsEnabled = isEnabled;
            Email = email;
            InternalId = internalId;
            Organisation = organisation;
            PasswordHash = passwordHash;
            PasswordSalt = passwordSalt;
        }

        public ApplicationUser(string firstName, string lastName, bool isEnabled, string email, Guid internalId, Organisation organisation, string passwordHash, string passwordSalt, ISet<PermissionAssignment> permissions, ISet<RoleAssignment> roles)
            : this(firstName, lastName, isEnabled, email, internalId, organisation, passwordHash, passwordSalt)
        {
            Permissions.UnionWith(permissions);
            Roles.UnionWith(roles);
        }
    }
}

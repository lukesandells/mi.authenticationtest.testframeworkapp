﻿using System;
using System.Collections.Generic;
using MI.AuthenticationTest.Models;

namespace MI.AuthenticationTest.Models
{
    public class SiteParameterType : ParameterType<Site>
    {
        //public virtual Site Site { get; set; }

        public SiteParameterType()
        {
        }

        public SiteParameterType(string name, string description, Guid internalId)
            : base(name, description, internalId/*, site*/)
        {
//            Value = site;
        }

        //public override string GetParameterStringValue(Parameter parameter)
        //    => (.InternalId.ToString();
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MI.AuthenticationTest.Models
{
    public class PermissionAssignmentParameter : Parameter
    {
        public virtual PermissionAssignment ParentPermissionAssignment { get; set; }
        public virtual string Value { get; set; }

        public PermissionAssignmentParameter()
        {
            Value = "";
        }

        public PermissionAssignmentParameter(string name, string description, Guid internalId, ParameterType type, string value) : base(name, description, internalId, type)
        {
            Value = value;
        }
    }
}


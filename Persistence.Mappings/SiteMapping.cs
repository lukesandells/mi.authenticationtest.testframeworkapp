﻿using System;
using System.Collections.Generic;
using System.Text;
using NHibernate.Mapping.ByCode.Conformist;
using MI.AuthenticationTest.Models;
using NHibernate.Mapping.ByCode;

namespace MI.AuthenticationTest.Persistence.Mappings
{
    public class SiteMapping : ClassMapping<Site>
    {
        public SiteMapping()
        {
            Id(s => s.SiteId, id =>
            {
                id.Access(Accessor.Property);
                id.Generator(Generators.HighLow, g => g.Params(new { max_lo = 1000 }));
            });

            Property(s => s.Name, prop => { prop.NotNullable(true); prop.Access(Accessor.Property); });
            Property(s => s.Description, prop => { prop.NotNullable(true); prop.Access(Accessor.Property); });
            Property(s => s.InternalId, prop => { prop.NotNullable(true); prop.Access(Accessor.Property); });

            ManyToOne(s => s.Organisation, manyToOne =>
            {
                manyToOne.Column("OrganisationId");
                manyToOne.Access(Accessor.Property);
                manyToOne.Fetch(FetchKind.Select);
                manyToOne.Lazy(LazyRelation.NoLazy);
                manyToOne.NotNullable(true);
                manyToOne.Cascade(Cascade.None);
            });
        }
    }
}

﻿using System;
using MI.AuthenticationTest.Models;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.AuthenticationTest.Persistence.Mappings
{
    public class OrganisationMapping : ClassMapping<Organisation>
    {
        public OrganisationMapping()
        {
            Lazy(false);

            Id(o => o.OrganisationId, id =>
            {
                id.Access(Accessor.Property);
                id.Generator(Generators.HighLow, g => g.Params(new { max_lo = 1000 }));
            });

            Property(o => o.Domain, property => { property.NotNullable(true); property.Access(Accessor.Property); });
            Property(o => o.InternalId, property => { property.NotNullable(true); property.Access(Accessor.Property); });
            Property(o => o.Name, property => { property.NotNullable(true); property.Access(Accessor.Property); });
        }
    }
}

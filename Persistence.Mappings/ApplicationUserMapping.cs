﻿using System;
using System.Collections.Generic;
using System.Text;
using MI.AuthenticationTest.Models;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.AuthenticationTest.Persistence.Mappings
{
    public class ApplicationUserMapping : ClassMapping<ApplicationUser>
    {
        public ApplicationUserMapping()
        {
            Id(u => u.UserId, id =>
            {
                id.Access(Accessor.Property);
                id.Generator(Generators.HighLow, g => g.Params(new { max_lo = 1000 }));
            });

            Property(u => u.FirstName, prop => { prop.NotNullable(true); prop.Access(Accessor.Property); });
            Property(u => u.LastName, property => { property.NotNullable(true); property.Access(Accessor.Property); });
            Property(u => u.InternalId, property => { property.NotNullable(true); property.Access(Accessor.Property); });
            Property(u => u.Email, property => { property.NotNullable(true); property.Access(Accessor.Property); });
            Property(u => u.IsEnabled, property => { property.NotNullable(true); property.Access(Accessor.Property); });
            Property(u => u.PasswordHash, property => { property.NotNullable(true); property.Access(Accessor.Property); });
            Property(u => u.PasswordSalt, property => { property.NotNullable(true); property.Access(Accessor.Property); });

            ManyToOne(o => o.Organisation, manyToOne =>
            {
                manyToOne.Column("OrganisationId");
                manyToOne.Access(Accessor.Property);
                manyToOne.Fetch(FetchKind.Select);
                manyToOne.Lazy(LazyRelation.NoLazy);
                manyToOne.NotNullable(true);
                manyToOne.Cascade(Cascade.None);
            });

            Set(u => u.Permissions, set =>
            {
                set.Table("UserPermissionAssignment");
                set.Key(k => k.Column("UserId"));
                set.Access(Accessor.Property);
                set.Fetch(CollectionFetchMode.Select);
            }, rel => rel.ManyToMany(r =>
            {
                r.Column("PermissionAssignmentId");
            }));

            Set(u => u.Roles, set =>
            {
                set.Table("UserRoleAssignment");
                set.Key(k => k.Column("UserId"));
                set.Access(Accessor.Property);
                set.Fetch(CollectionFetchMode.Select);
            }, rel => rel.ManyToMany(r =>
            {
                r.Column("RoleAssignmentId");
            }));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using MI.AuthenticationTest.Models;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.AuthenticationTest.Persistence.Mappings
{
    public class ParameterTypeMapping : ClassMapping<ParameterType>
    {
        public ParameterTypeMapping()
        {
            Id(sp => sp.ParameterTypeId, id =>
                {
                    id.Access(Accessor.Property);
                    id.Generator(Generators.HighLow, g => g.Params(new { max_lo = 1000 }));
                });

            Property(s => s.Name, property => { property.NotNullable(true); property.Access(Accessor.Property); });
            Property(s => s.Description, property => { property.NotNullable(true); property.Access(Accessor.Property); });
            Property(s => s.InternalId, property => { property.NotNullable(true); property.Access(Accessor.Property); });

            Discriminator(d => { d.Column("TypeDiscriminator"); });
        }
    }
}

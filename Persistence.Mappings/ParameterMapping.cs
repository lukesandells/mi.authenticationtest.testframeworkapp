﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.AuthenticationTest.Models.Mapping
{
    public class ParameterMapping : ClassMapping<Parameter>
    {
        public ParameterMapping()
        {
            Id(p => p.ParameterId, id =>
            {
                id.Access(Accessor.Property);
                id.Generator(Generators.HighLow, g => g.Params(new { max_lo = 1000 }));
            });

            Property(p => p.Name, prop => { prop.NotNullable(true); prop.Access(Accessor.Property); });
            Property(p => p.Description, prop => { prop.NotNullable(true); prop.Access(Accessor.Property); });
            Property(p => p.InternalId, prop => { prop.NotNullable(true); prop.Access(Accessor.Property); });

            ManyToOne(p => p.Type, manyToOne =>
            {
                manyToOne.Column("ParameterTypeId");
                manyToOne.Access(Accessor.Property);
                manyToOne.Fetch(FetchKind.Select);
                manyToOne.Lazy(LazyRelation.NoLazy);
                manyToOne.NotNullable(false);
                manyToOne.Cascade(Cascade.None);
            });

            Discriminator(d => { d.Column("Discriminator"); });
        }
    }

    public class RoleAssignmentParameterMapping : SubclassMapping<RoleAssignmentParameter>
    {
        public RoleAssignmentParameterMapping()
        {
            DiscriminatorValue("RoleAssignmentParameter");

            Property(p => p.Value, prop => { prop.NotNullable(false); prop.Access(Accessor.Property); });

            //ManyToOne(p => p.ParentRoleAssignment, manyToOne =>
            //{
            //    manyToOne.Update(true);
            //    manyToOne.Column("RoleAssignmentId");
            //    manyToOne.Access(Accessor.Property);
            //    manyToOne.Fetch(FetchKind.Select);
            //    manyToOne.Lazy(LazyRelation.NoLazy);
            //    manyToOne.NotNullable(false);
            //    manyToOne.Cascade(Cascade.None);
            //});
        }
    }

    public class RoleTemplateParameterMapping : SubclassMapping<RoleTemplateParameter>
    {
        public RoleTemplateParameterMapping()
        {
            DiscriminatorValue("RoleTemplateParameter");

            Property(p => p.DefaultValue, prop => { prop.NotNullable(false); prop.Access(Accessor.Property); });

            //ManyToOne(p => p.ParentRoleTemplate, manyToOne =>
            //{
            //    manyToOne.Update(true);
            //    manyToOne.Column("RoleTemplateId");
            //    manyToOne.Access(Accessor.Property);
            //    manyToOne.Fetch(FetchKind.Select);
            //    manyToOne.Lazy(LazyRelation.NoLazy);
            //    manyToOne.NotNullable(false);
            //    manyToOne.Cascade(Cascade.None);
            //});
        }
    }

    public class PermissionTemplateParameterMapping : SubclassMapping<PermissionTemplateParameter>
    {
        public PermissionTemplateParameterMapping()
        {
            DiscriminatorValue("PermissionTemplateParameter");

            Property(p => p.DefaultValue, prop => { prop.NotNullable(false); prop.Access(Accessor.Property); });

            //ManyToOne(p => p.ParentPermissionTemplate, manyToOne =>
            //{
            //    manyToOne.Update(true);
            //    manyToOne.Column("PermissionTemplateId");
            //    manyToOne.Access(Accessor.Property);
            //    manyToOne.Fetch(FetchKind.Select);
            //    manyToOne.Lazy(LazyRelation.NoLazy);
            //    manyToOne.NotNullable(false);
            //    manyToOne.Cascade(Cascade.None);
            //});
        }
    }

    public class PermissionAssignmentParameterMapping : SubclassMapping<PermissionAssignmentParameter>
    {
        public PermissionAssignmentParameterMapping()
        {
            DiscriminatorValue("PermissionAssignmentParameter");

            Property(p => p.Value, prop => { prop.NotNullable(false); prop.Access(Accessor.Property); });

            //ManyToOne(p => p.ParentPermissionAssignment, manyToOne =>
            //{
            //    manyToOne.Update(true);
            //    manyToOne.Column("PermissionAssignmentId");
            //    manyToOne.Access(Accessor.Property);
            //    manyToOne.Fetch(FetchKind.Select);
            //    manyToOne.Lazy(LazyRelation.NoLazy);
            //    manyToOne.NotNullable(false);
            //    manyToOne.Cascade(Cascade.None);
            //});
        }
    }
}

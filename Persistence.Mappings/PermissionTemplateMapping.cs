﻿using System;
using System.Collections.Generic;
using System.Text;
using MI.AuthenticationTest.Models;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.AuthenticationTest.Persistence.Mappings
{
    public class PermissionTemplateMapping : ClassMapping<PermissionTemplate>
    {
        public PermissionTemplateMapping()
        {
            Id(pt => pt.PermissionTemplateId, id =>
            {
                id.Access(Accessor.Property);
                id.Generator(Generators.HighLow, g => g.Params(new {max_lo = 1000}));
            });

            Property(pt => pt.Name, prop => { prop.Access(Accessor.Property); prop.NotNullable(true); });
            Property(pt => pt.ClaimType, prop => { prop.NotNullable(true); prop.Access(Accessor.Property); });
            Property(pt => pt.Description, prop => { prop.NotNullable(true); prop.Access(Accessor.Property); });
            Property(pt => pt.InternalId, prop => { prop.NotNullable(true); prop.Access(Accessor.Property); });

            //ManyToOne(pt => pt.RoleTemplate, manyToOne =>
            //{
            //    manyToOne.Column("RoleTemplateId");
            //    manyToOne.Access(Accessor.Property);
            //    manyToOne.Fetch(FetchKind.Select);
            //    manyToOne.Lazy(LazyRelation.NoLazy);
            //    manyToOne.NotNullable(false);
            //    manyToOne.Cascade(Cascade.None);
            //});

            //Set(pt => pt.TemplateParameters, set =>
            //{
            //    set.Table("PermissionTemplateTemplateParameter");
            //    set.Key(k => k.Column("PermissionTemplateId"));
            //    set.Access(Accessor.Property);
            //    set.Fetch(CollectionFetchMode.Select);
            //}, rel => rel.ManyToMany(r =>
            //{
            //    r.Column("ParameterId");
            //}));
            
            Set(pt => pt.TemplateParameters, set =>
            {
                set.Inverse(false);
                set.Key(k => k.Column("PermissionTemplateId"));
                set.Access(Accessor.Property);
                set.Fetch(CollectionFetchMode.Select);
            }, rel => rel.OneToMany());
        }
    }
}

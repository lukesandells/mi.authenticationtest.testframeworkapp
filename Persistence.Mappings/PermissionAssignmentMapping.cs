﻿using System;
using System.Collections.Generic;
using System.Text;
using MI.AuthenticationTest.Models;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.AuthenticationTest.Persistence.Mappings
{
    public class PermissionAssignmentMapping : ClassMapping<PermissionAssignment>
    {
        public PermissionAssignmentMapping()
        {
            Id(pa => pa.PermissionAssignmentId, id => 
            {
                id.Access(Accessor.Property);
                id.Generator(Generators.HighLow, gen => gen.Params(new { max_lo = 1000 }));
            });

            Property(pa => pa.Name, prop => { prop.Access(Accessor.Property); prop.NotNullable(true); });
            Property(pa => pa.ClaimType, prop => { prop.Access(Accessor.Property); prop.NotNullable(true); });
            Property(pa => pa.Description, prop => { prop.Access(Accessor.Property); prop.NotNullable(true); });
            Property(pa => pa.InternalId, prop => { prop.Access(Accessor.Property); prop.NotNullable(true); });

            ManyToOne(pa => pa.PermissionTemplate, manyToOne =>
            {
                manyToOne.Column("PermissionTemplateId");
                manyToOne.Access(Accessor.Property);
                manyToOne.Fetch(FetchKind.Select);
                manyToOne.Lazy(LazyRelation.NoLazy);
                manyToOne.NotNullable(true);
                manyToOne.Cascade(Cascade.None);
            });

            ManyToOne(pa => pa.RoleAssignment, manyToOne =>
            {
                manyToOne.Column("RoleAssignmentId");
                manyToOne.Access(Accessor.Property);
                manyToOne.Fetch(FetchKind.Select);
                manyToOne.Lazy(LazyRelation.NoLazy);
                manyToOne.NotNullable(false);
                manyToOne.Cascade(Cascade.None);
            });

            //Set(pa => pa.PermissionParameters, set =>
            //{
            //    set.Table("PermissionAssignmentPermissionParameter");
            //    set.Key(k => k.Column("PermissionAssignmentId"));
            //    set.Access(Accessor.Property);
            //    set.Fetch(CollectionFetchMode.Select);
            //}, rel => rel.ManyToMany(r =>
            //{
            //    r.Column("ParameterId");
            //}));

            Set(pa => pa.PermissionParameters, set =>
            {
                set.Inverse(false);
                set.Key(k => k.Column("PermissionAssignmentId"));
                set.Access(Accessor.Property);
                set.Fetch(CollectionFetchMode.Select);
            }, rel => rel.OneToMany());
        }
    }
}

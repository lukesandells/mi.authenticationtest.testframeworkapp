﻿using System;
using System.Collections.Generic;
using System.Text;
using MI.AuthenticationTest.Models;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.AuthenticationTest.Persistence.Mappings
{
    public class RefreshTokenMapping : ClassMapping<RefreshToken>
    {
        public RefreshTokenMapping()
        {
            Id(o => o.Id, id =>
            {
                id.Access(Accessor.Property);
                id.Generator(Generators.HighLow, g => g.Params(new { max_lo = 1000 }));
            });

            //public virtual DateTime IssuedUtc { get; set; }
            //public virtual DateTime ExpiresUtc { get; set; }
            //public virtual string Token { get; set; }

            //public virtual ApplicationUser User { get; set; }
            Property(t => t.IssuedUtc, prop => { prop.NotNullable(true); prop.Access(Accessor.Property); });
            Property(t => t.ExpiresUtc, prop => { prop.NotNullable(true); prop.Access(Accessor.Property); });
            Property(t => t.Token, prop => { prop.NotNullable(true); prop.Access(Accessor.Property); });

            ManyToOne(t => t.User, manyToOne =>
            {
                manyToOne.Column("UserId");
                manyToOne.Access(Accessor.Property);
                manyToOne.Fetch(FetchKind.Select);
                manyToOne.Lazy(LazyRelation.NoLazy);
                manyToOne.NotNullable(true);
                manyToOne.Cascade(Cascade.None);
            });
        }
    }
}

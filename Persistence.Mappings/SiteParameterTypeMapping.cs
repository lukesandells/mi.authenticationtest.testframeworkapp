﻿using System;
using System.Collections.Generic;
using System.Text;
using MI.AuthenticationTest.Models;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.AuthenticationTest.Persistence.Mappings
{
    public class SiteParameterTypeMapping : SubclassMapping<SiteParameterType>
    {
        public SiteParameterTypeMapping()
        {
            DiscriminatorValue("SiteParameterType");

            //ManyToOne(s => s.Value, manyToOne =>
            //{
            //    manyToOne.Column("SiteId");
            //    manyToOne.Access(Accessor.Property);
            //    manyToOne.Fetch(FetchKind.Select);
            //    manyToOne.Lazy(LazyRelation.NoLazy);
            //    manyToOne.NotNullable(false);
            //    manyToOne.Cascade(Cascade.None);
            //});

            //Set(s => s.ParameterValue, set =>
            //{
            //    set.Table("SitePermissionParameterParameterValue");
            //    set.Key(k => k.Column("SiteId"));
            //    set.Access(Accessor.Property);
            //    set.Fetch(CollectionFetchMode.Select);
            //}, rel => rel.ManyToMany(r =>
            //{
            //    r.Column("ParameterTypeId");
            //}));
        }
    }
}

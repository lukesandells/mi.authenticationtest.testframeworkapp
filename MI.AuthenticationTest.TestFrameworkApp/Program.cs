﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MI.AuthenticationTest.Models;
using MI.AuthenticationTest.Persistence.NHibernate.Postgres;
using MI.Framework.Persistence;
using MI.Framework.ObjectBrowsing;

namespace MI.AuthenticationTest.TestFrameworkApp
{
    class Program
    {
        static void Main(string[] args)
        {
            PostgresDatabase.Connect("localhost", 5432, "MI.AuthenticationTest", "postgres", "manint", true, true);

            //put things in
            List<Guid> usedGuids = new List<Guid>();
            List<Organisation> orgs = new List<Organisation>();

            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var org1 = new Organisation("NameTest", "DomainTest", Guid.NewGuid());
                var org2 = new Organisation("Second test", "Luke", Guid.NewGuid());
                var org3 = new Organisation("User", "12345", Guid.NewGuid());
                usedGuids.Add(org1.InternalId);
                usedGuids.Add(org2.InternalId);
                usedGuids.Add(org3.InternalId);

                orgs.Add(org1);
                orgs.Add(org2);
                orgs.Add(org3);
                
                scope.Persist(org1);
                scope.Persist(org2);
                scope.Persist(org3);

                scope.Done();
            }

            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var user1 = new ApplicationUser("User", "McTest", true, "test@emailplaceserver.com", Guid.NewGuid(), orgs[0]);
                var user2 = new ApplicationUser("Luke", "Sandells", false, "luke.sandells@manufacturingintelligence.com.au", Guid.NewGuid(), orgs[1]);
                var user3 = new ApplicationUser("", "", true, "", Guid.NewGuid(), orgs[2]);

                scope.Persist(user1);
                scope.Persist(user2);
                scope.Persist(user3);

                scope.Done();
            }

            Console.WriteLine();

            //get things out and print
            using (var scope = new PersistenceScope(TransactionOption.None))
            {
                var orgList = scope.ListAll<Organisation>();
                foreach (Organisation o in orgList)
                {
                    Console.WriteLine($"{o.Name} {o.Domain} : {o.OrganisationId} {o.InternalId}");
                }

                var usrList = scope.ListAll<ApplicationUser>();
                foreach (ApplicationUser u in usrList)
                {
                    Console.WriteLine($"{u.FirstName} {u.LastName} {u.Email} {u.Organisation.OrganisationId} {u.IsEnabled} : {u.UserId} {u.InternalId}");
                }

                scope.Done();
            }

            Console.WriteLine("Done");
            Console.ReadLine();
        }
    }
}
